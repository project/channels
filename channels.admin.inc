<?php
// $Id: channels.admin.inc,v 1.3 2011/01/10 16:38:04 abouillis Exp $
/**
 * @file
 * Administration UI description
 * 
 */

/**
 * Form callback showing a form for adding a channel.
 */
function channels_admin_channel_add(array $form, array &$form_state) {

  drupal_set_title(t('Add a new channel'));

  $ct = empty($form_state['values']['ct']) ? '' : $form_state['values']['ct'];

  if (empty($form_state['storage']['step_one'])) {

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Channel name'),
      '#description' => t('Enter the displayed name for the new channel.'),
      '#maxlength' => 50,
      '#required' => TRUE,
      '#weight' => 0
    );
    
    //$form['name'] = form_process_machine_name($form['name'], $form_state);
    
    $form['machine_name'] = array(
      '#type' => 'machine_name',
      '#description' => t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
      '#maxlength' => 50,
      '#required' => TRUE,
      '#weight' => 0,
      '#machine_name' => array(
        'source' => array('name'),
        'label' => t('Machine-readable name'),
        'pattern' => '[^a-z0-9_]+',
        'replace' => '_',
        'exists' => 'channels_channel_exists',
      )
    );
   
    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Channel description'),
      '#description' => t('Enter a description for the new channel.'),
      '#weight' => 1,
    );
  }
  elseif (!$ct) {
    $ct = $form_state['storage']['step_one']['ct'];
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create channel'),
    '#weight' => 5,
  );
  
  return $form;
}

/**
 * AJAX callback that just returns the content types "options" array of the already built form
 * array.
 */
function channels_admin_stream_add_callback(array $form, array &$form_state) {
  //return $form['ajax_form'];
  return $form['ajax_form']['bundle_name'];
}


/**
 * 
 * Get the given form and modify it to select a service
 * @param array $form
 * @param array $form_state
 * @param array $values Forced form values
 */
function _channels_admin_service_form(array $form, array &$form_state, array $values = array()) {

  if ( isset($values['service']) )
    $form_state['values']['service'] = $values['service']; 


  $service = empty($form_state['values']['service']) ? '' : $form_state['values']['service'];

  $form['service'] = array(
    '#type' => 'select',
    '#title' => t('Service'),
    '#description' => t('Choose the service wich will be used to transfer the selected bundle of data.'),
    '#options' => array('' => '< ' . t('Choose a service') . ' >'),
    '#required' => TRUE,
    '#default_value' => $service,
  );
  
  if ($service != '') $form['service']['#disabled'] = TRUE;

  $services = channels_get_services();

  if ( count($services)>0 ) {  
    foreach ($services as $key => $label) {  
     $form['service']['#options'][$key] = $label; 
    }
  }
  else{
    $form['service']['#options'] = array('' => '< ' . t('No service available') . ' >');
  }

  return $form;
}


/**
 * 
 * Return the fields as a checkboxes list in a form for a given content type
 * 
 * @param array $form
 * @param array $form_state
 * @param array $values Forced form values
 */
function _channels_admin_bundle_fields_node_form(array $form, array &$form_state, array $values = array()) {

  $type = $form_state['values']['type'];
  $bundle = $form_state['values']['bundle'];

  $entity_fields = field_read_instances(array('bundle' => $bundle));

  $info_bundles = field_info_bundles($type);


  $node_properties = channels_node_properties_info();
    
  $form['properties'] = array(
   '#type' => 'checkboxes',
   '#required' => TRUE,
   '#title' => t('Native fields'),
   '#weight' => 2,
  );
    
  foreach ($node_properties as $key => $value)
    $form['properties']['#options'][$key] = $value . ' ( ' . $key . ' ) ';


  if ( count($entity_fields) > 0 ) {

    $form[$bundle . '-fields'] = array(
      '#prefix' => '<label>' . t('@bundle fields', array('@bundle' => $info_bundles[$bundle]['label'])) . '</label>',
      '#type' => 'tableselect',
      '#tree' => TRUE,
      '#header' => array(
        'label' => t('LABEL'), 
        'field_name' => t('NAME'),
        'type' => t('TYPE'),
        'module' => t('MODULE'),
      ),
      '#title' => t('Fields available'),
      '#weight' => 2,
    );

    foreach ($entity_fields as $ef) {

      $field_info = field_info_field_by_id($ef['field_id']);

      // set checkbox input for the current field
      $form[$bundle . '-fields']['#options'][$ef['field_name']] = array(
        'label' => $ef['label'], 
        'field_name' => $ef['field_name'],
        'type' => $field_info['type'],
        'module' => $field_info['module'],
      );  

      // pre-check the box if the field is required
      if ( $ef['required'] == 1 ) {
        $form[$bundle . '-fields']['#options'][$ef['field_name']]['label'] =  $ef['label'] . ' ' . t('(required)');
        $form[$bundle . '-fields']['#default_value'][$ef['field_name']] = TRUE;
      }  
      else { 
        $form[$bundle . '-fields']['#default_value'][$ef['field_name']] = FALSE;
      }
    }
  }
 
  // set properties forced values
  if ( isset($values['properties']) && count($values['properties'])>0) {   
      foreach ($form['properties']['#options'] as $key => $value) {
        if ( in_array($key, $values['properties']) )$form['properties']['#default_value'][] = $key;
      }
  }
  
  // set fields forced values
  if ( isset($values['fields']) && count($values['fields'])>0) {   
      foreach ($form[$bundle . '-fields']['#options'] as $key => $value) {
        if ( in_array($key, $values['fields']) )$form[$bundle . '-fields']['#default_value'][$key] = TRUE;
      }
  }
  return $form;
}



/**
 * Page callback that shows an overview of defined channels.
 */
function channels_admin_overview() {

  $base_path = drupal_get_path('module', 'channels') . '/';
  drupal_add_css($base_path . 'channels.admin.css');
  $pre = CHANNELS_MENU_BASEPATH;

  $header = array(
    array('data' => t('Status'), 'field' => 'status', 'sort' => 'desc'),
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Machine name'), 'field' => 'machine_name'),
    array('data' => t('Description'), 'field' => 'description', 'class' => array('desc')),
    array('data' => t('Nb elements')),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  
  $channels = channels_get_channels(false, $header);
  
  
  $t_enabled['data'] = array(
    '#theme' => 'image',
    '#path' => $base_path . 'img/enabled.png',
    '#alt' => t('Enabled'),
    '#title' => t('Enabled'),
  );
  $t_enabled['class'] = array('channel-status');
  $t_disabled['data'] = array(
    '#theme' => 'image',
    '#path' => $base_path . 'img/disabled.png',
    '#alt' => t('Disabled'),
    '#title' => t('Disabled'),
  );
  $t_disabled['class'] = array('channel-status');
  
  $rows = array();
  
  foreach ($channels as $channel) {

    $status = $t_disabled;
    if ( $channel->status==1 ) $status = $t_enabled;

      $rows[] = array(

      // enable /disable
      $status,

      // channel name
      l($channel->name, $pre . '/channel/' . $channel->id . '/view'),
      
      // channel machine name
      $channel->machine_name,
      
      // channel description
      $channel->description,
      
      array('data' => $channel->getNbStreams(), 'class' => 'info-center'),
      
      // channel edit link
      l(t('Edit'), $pre . '/channel/' . $channel->id . '/edit'),
      
      // channel delete link
      l(t('Delete'), $pre . '/channel/' . $channel->id . '/delete'),
    );
    
  }
  
  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no channels defined yet.'),
  );
}


/**
 * Form submit callback for adding a channel.
 */
function channels_admin_channel_add_submit(array $form, array &$form_state) {

  form_state_values_clean($form_state);
    
  $id = channels_channel_insert($form_state['values']);
  
  if ( $id > 0)
    drupal_set_message(t('The channel was successfully created.'));
  else
    drupal_set_message(t('An error occured during the channel creation.'), 'error');  
  
  $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/channel/' . $id;
  
  
  return;
}


/**
 * Helper function for displaying a generic confirmation form.
 *
 * @return
 *   Either a form array, or FALSE if this combination of type and action is
 *   not supported.
 */
function channels_admin_confirm(array $form, array &$form_state, $type, $action, $info) {

  switch ($type) {
    case 'channel':
      switch ($action) {
        case 'enable':
          $text = array(
            t('Enable channel "@name"', array('@name' => $info->name)),
            t('Do you really want to enable this channel?'),
            t('This will NOT enable all associated tasks.'),
            t('The channel was successfully enabled.'),
          );
          break;
        case 'disable':
          $text = array(
            t('Disable channel "@name"', array('@name' => $info->name)),
            t('Do you really want to disable this channel?'),
            t('This will disable both the server and all associated synchronization.'),
            t('The channel was successfully disabled.'),
          );
          break;
        case 'delete':
          if ( !channels_admin_confirm_delete($info->id) ) return FALSE;
          $text = array(
            t('Delete channel "@name"', array('@name' => $info->name)),
            t('Do you really want to delete this channel?'),
            t('This will delete the channel and associated synchronizations.'),
            t('The channel was successfully deleted.'),
          );
          break;
        default:
          return FALSE;
      }
      break;
    case 'stream':
      switch ($action) {
        case 'enable':
          $text = array(
            t('Enable channel stream "@name"', array('@name' => $info->name)),
            t('Do you really want to enable this stream?'),
            t(""),
            t('The channel stream was successfully enabled.'),
          );
          break;
        case 'disable':
          $text = array(
            t('Disable channel stream "@name"', array('@name' => $info->name)),
            t('Do you really want to disable this stream?'),
            t(""),
            t('The channel stream was successfully disabled.'),
          );
          break;
        case 'delete':
          $text = array(
            t('Delete channel stream "@name"', array('@name' => $info->name)),
            t('Do you really want to delete this channel stream?'),
            t('This will disable the current stream synchronization.'),
            t('The channel stream has been successfully deleted.'),
          );
          break;
        default:
          return FALSE;
      }
      break;
    default:
      return FALSE;
  }

  
  $desc = "<h3>{$text[1]}</h3><p>{$text[2]}</p>";
  
  $form = array(
    'type' => array(
      '#type' => 'value',
      '#value' => $type,
    ),
    'action' => array(
      '#type' => 'value',
      '#value' => $action,
    ),
    'id' => array(
      '#type' => 'value',
      '#value' => $info->id,
    ),
    'message' => array(
      '#type' => 'value',
      '#value' => $text[3],
    ),
  );  
  
  if ( $type != 'stream') return confirm_form($form, $text[0], CHANNELS_MENU_BASEPATH . "/$type/{$info->id}", $desc);
  
  return confirm_form($form, $text[0], CHANNELS_MENU_BASEPATH . "/channel/{$info->channel_id}", $desc);
  
}


/**
 *  Helper function for displaying an alert if the current channel can not be deleted.
 *  (some nodes are assigned to it)
 *  
 *  @param int $id Channel id
 *  @return bool TRUE if the channel can be deleted, else FALSE
 */
function channels_admin_confirm_delete($id) {

  $nodes = channels_get_nodes_by_channel($id);

  if (count($nodes)>0) {
    drupal_set_message(t('This channel can\'t be deleted because some nodes are still assigned to it.'), 'error');
    return FALSE;
  }
  return TRUE;
}


/**
 * Submit function for search_api_admin_confirm().
 */
function channels_admin_confirm_submit(array $form, array &$form_state) {

  form_state_values_clean($form_state);

  $values = $form_state['values'];

  $type = $values['type'];
  $action = $values['action'];
  $id = $values['id'];
  
  if ( $type == 'stream')
     $stream = channel_stream_load($id);  
  
  $function = "channels_{$type}_{$action}";
  
  if ($function($id)) {
    drupal_set_message($values['message']); 
  }
  else {
    drupal_set_message(t('An error has occurred while performing the desired action. Check the logs for details.'), 'error');
  }
  
  if ( $type == 'stream') {
    $form_state['redirect'] = CHANNELS_MENU_BASEPATH . "/channel/$stream->channel_id";
  }
  else
    $form_state['redirect'] = ($action == 'delete') ? CHANNELS_MENU_BASEPATH : CHANNELS_MENU_BASEPATH . "/$type/$id";
}


/**
 * Title callback for viewing or editing a channel or index.
 */
function channels_admin_item_title($object) {
  return $object->name;
}


/**
 * Edit a channel's settings.
 *
 * @param object $channel
 *   The server to edit.
 */
function channels_admin_channel_edit(array $form, array &$form_state, Channel $channel) {

  $title = t('Edit channel');
  drupal_set_title($title . ' "' . $channel->name . '"');

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Channel name'),
    '#description' => t('Enter the displayed name for the channel.'),
    '#maxlength' => 50,
    '#default_value' => $channel->name,
    '#required' => TRUE,
  );
  
  $form['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine-name'),
    '#description' => t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
    '#maxlength' => 50,
    '#default_value' => $channel->machine_name,
    '#required' => TRUE,
    '#disabled' => TRUE,
  );
  
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Channel description'),
    '#description' => t('Enter a description for the channel.'),
    '#default_value' => $channel->description,
  );

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $channel->id,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}


/**
 * Validation function for channels_admin_channel_edit.
 */
function channels_admin_channel_edit_validate(array $form, array &$form_state) {

}

/**
 * Submit function for channels_admin_channel_edit.
 */
function channels_admin_channel_edit_submit(array $form, array &$form_state) {
  form_state_values_clean($form_state);

  $values = $form_state['values'];
  $id = $values['id'];
  unset($values['id']);

  $ret = channels_channel_edit($id, $values);
  $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/' . $id;
  if ($ret) {
    drupal_set_message(t('The channel was successfully edited.'));
  }
  else {
    drupal_set_message(t('No values were changed.'));
  }
}


/**
 * Displays a channel's details.
 *
 * @param  $server
 *   The channel to display.
 * @param $action
 *   One of 'enable', 'disable', 'delete'; or NULL if the channel is only viewed.
 */
function channels_admin_channel_view(Channel $channel = NULL, $action = NULL) {

  drupal_set_title(t('Channel "@name"', array('@name' => $channel->name) ) );

  if (empty($channel)) {
    return MENU_NOT_FOUND;
  }

  if (!empty($action)) {
    if ( $action == 'enable' || $action == 'disable') {
      return drupal_get_form('channels_admin_confirm', 'channel', $action, $channel);
    }
  }
  
  $streams = $channel->fetchStreams();
  $nb_elt = count($streams);
  
  $errors = array();
  
  for ($i=0; $i<$nb_elt; $i++) {

    $fields_info = field_info_bundles($streams[$i]->type);


    // check if the service is known
    /*if ( in_array($streams[$i]->service, array_keys($services)) ) {
      $streams[$i]->service = $services[$streams[$i]->service];
    }
    elseif ( !in_array($streams[$i]->service, $errors) ) {
        $errors[$streams[$i]->service] = 'service'; 
    }*/

    // check if the bundle exists
    if ( !isset($fields_info[$streams[$i]->bundle]) ) {
      if ( !in_array($streams[$i]->bundle, $errors) )
        $errors[$streams[$i]->bundle] = 'bundle'; 
    }
    elseif ( $streams[$i]->type == 'node' )
      $streams[$i]->bundle = $fields_info[$streams[$i]->bundle]['label'];
  }

  if ( count($errors)>0 ) {
    foreach ($errors as $key => $item) {
      $title = t('Unknown ' . $item);
      drupal_set_message($title . ' "' . $key . '"', 'error');
    }
  }
  
  return array(
    '#theme' => 'channels_admin_channel_view',
    '#id' => $channel->id,
    '#name' => $channel->name,
    '#machine_name' => $channel->machine_name,
    '#description' => $channel->description,
    '#status' => $channel->status,
    '#streams' => $streams,
    //'#streams' => drupal_get_form('channels_admin_stream_list_form', $streams),
  );
}


/**
 * Return the channel streams list.
 *
 * @param object $stream
 *   The server to edit.
 */
function channels_admin_stream_list(array $form, array &$form_state, $channel_id) {

  $channel = channel_load($channel_id);

  $header = array(
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Description'), 'field' => 'description', 'class' => array('desc')),
    array('data' => t('Last execution time')),
    array('data' => t('Status'), 'field' => 'status'),
    array('data' => t('Type of stream'), 'field' => 'type'),
    array('data' => t('Bundle'), 'field' => 'bundle'),
    //array('data' => t('Service'), 'field' => 'service'),
    array('data' => t('Priority'), 'field' => 'priority', 'sort' => 'asc'),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  
  $streams = $channel->fetchStreams(FALSE, $header);

  if ( count($streams)>0 ) {
    foreach ($streams as $key => $str) {

      $form['streams'][$str->id]['priority-' . $str->id] = array(
        '#type' => 'textfield',
        '#size' => 3,
        '#attributes' => array('class' => array('priority')),
        '#default_value' => $str->priority,
      );
      
      $form['streams'][$str->id]['data'] = array(
        '#type' => 'value',
        '#value' => array(
          'id' => $str->id,
          'name' => $str->name,
          'description' => $str->description,
          'last_sync' => $str->last_sync,
          'status' => $str->status,
          'type' => $str->type,
          'bundle' => $str->bundle,
          //'service' => $str->service,
        )
      );
    }
    $form['streams']['#theme'] = 'channels_admin_stream_list_item';
    $form['channel'] = array(
      '#type' => 'hidden',
      '#value' => $channel_id,
    );
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
    );
  }
  return $form;
}


/**
 * Validation function for channels_admin_stream_list_form (streams priorities validation).
 */
function channels_admin_stream_list_submit(array $form, array &$form_state) {
  
  form_state_values_clean($form_state);
  $error = FALSE;
  
  foreach ($form_state['values'] as $key => $data) {
    //we are only interested in priority elements
    if (substr($key, 0, 8)=='priority') {
      $id = str_replace('priority-', '', $key);
      $ret = channels_stream_edit_priority($id, $data);
      if (!$ret) {
        $mes = t('An error occured during the priority change a stream');
        drupal_set_message($mes . '(' . $key . ')', 'error');
        $error = TRUE;  
      } 
    }
  }

  if (!$error) drupal_set_message(t('All the streams\' priorities was successfully changed.'));
  $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/channel/' . $form_state['values']['channel'];
}

/**
 * Theme function for displaying the stream list for a channel.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: The form (table) of the streams (needed to re-order the synchronization priority).
 *   - streams: an array of ChannelStream object, the streams to display.   
 */
function theme_channels_admin_stream_list_item(array $variables) {

  extract($variables);

  $base_path = drupal_get_path('module', 'channels') . '/';

  $table_rows = array();

  $services = channels_get_services();

  foreach (element_children($form) as $key) {

    $data = $form[$key]['data']['#value'];

    $info_bundles = field_info_bundles($data['type']);
    
    if ( isset($info_bundles[$data['bundle']]['label']))
      $data['bundle'] = $info_bundles[$data['bundle']]['label'];
    else
      $data['bundle'] = '<span style="color:red;">' . $data['bundle'] . '</span>';    

    /*if ( in_array($data['service'], array_keys($services) ) )
      $data['service'] = $services[$data['service']];
    else
      $data['service'] = '<span style="color:red;">' . $data['service'] . '</span>';
    */
        
    if ( is_null($data['last_sync']) ) $data['last_sync'] = t('Never');
    
    if ($data['status'] == 1) {
      $data['status'] = array(
        'data' => t('enabled (!disable_link)', array('!disable_link' => l(t('disable'), CHANNELS_MENU_BASEPATH . '/stream/' . $data['id'] . '/disable'))),
        'class' => array('enabled'),
      );
    }
    elseif ($data['status'] == 0) {
      $data['status'] = array(
        'data' => t('disabled (!enable_link)', array('!enable_link' => l(t('enable'), CHANNELS_MENU_BASEPATH . '/stream/' . $data['id'] . '/enable'))),
        'class' => array('disabled'),
      );
    }
    
    $more_data = array(
      'priority' => array('data' => drupal_render($form[$key]['priority-' . $data['id']]), 'class' => array('info-center')),
      'edit_link' => l(t('Edit'), CHANNELS_MENU_BASEPATH . '/stream/' . $data['id'] . '/edit'),
      //'settings_link' => l(t('Settings'), CHANNELS_MENU_BASEPATH . '/stream/' . $data['id'] . '/settings'),
      'delete_link' => l(t('Delete'), CHANNELS_MENU_BASEPATH . '/stream/' . $data['id'] . '/delete'),
    );
    
    unset($data['id']);
    
    $table_rows[] = array(
      'data' => array_merge($data, $more_data), 
      'class' => array('draggable'),
    );
  }

  $header = array(
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Description'), 'field' => 'description', 'class' => array('desc')),
    array('data' => t('Last execution time')),
    array('data' => t('Status'), 'field' => 'status', 'sort' => 'desc'),
    array('data' => t('Type of stream'), 'field' => 'type'),
    array('data' => t('Bundle'), 'field' => 'bundle'),
    //array('data' => t('Service'), 'field' => 'service'),
    array('data' => t('Priority'), 'field' => 'priority', 'sort' => 'asc'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  drupal_add_tabledrag('streams-list', 'order', 'slibing', 'priority');
  
  $output = '';
  
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $table_rows,
    'attributes' => array('id' => 'streams-list'),
    'empty' => t('There are no stream defined yet.'),
  ));
  
  return $output;  
}

/**
 * Theme function for displaying a channel.
 *
 * @param array $variables
 *   An associative array containing:
 *   - id: The channel's id.
 *   - name: The channel's name.
 *   - machine-name: The channel's machine-name
 *   - description: The channel's description.
 *   - status: Boolean indicating whether the channel is enabled.
 */
function theme_channels_admin_channel_view(array $variables) {
  
  $base_path = drupal_get_path('module', 'channels') . '/';
  drupal_add_css($base_path . 'channels.admin.css', array('group' => CSS_THEME));

  extract($variables);
  
  $output = '';

  $output .= '<h3>' . check_plain($name) . '</h3>' . "\n";

  $output .= '<dl>' . "\n";

  $output .= '<dt class="form-label">' . t('Machine-name') . '</dt>' . "\n";
  
  $output .= '<dd>' . $machine_name . "\n";
  
  $output .= '<dt class="form-label">' . t('Status') . '</dt>' . "\n";
  $output .= '<dd';
  
  if ($status == 1)
    $output .= ' class="enabled">' . t('enabled (!disable_link)', array('!disable_link' => l(t('disable'), CHANNELS_MENU_BASEPATH . '/channel/' . $id . '/disable')));
  if ($status == 0)
    $output .= ' class="disabled">' . t('disabled (!enable_link)', array('!enable_link' => l(t('enable'), CHANNELS_MENU_BASEPATH . '/channel/' . $id . '/enable')));

  $output .= '</dd>' . "\n";

  if (!empty($description)) {
    $output .= '<dt class="form-label-cr">' . t('Description') . '</dt>' . "\n";
    $output .= '<dd>' . nl2br(check_plain($description)) . '</dd>' . "\n";
  }

  if (!empty($class_name)) {
    $output .= '<dt>' . t('Service class') . '</dt>' . "\n";
    $output .= '<dd><em>' . check_plain($class_name) . '</em>';
    if (!empty($class_description)) {
      $output .= '<p class="description">' . nl2br(check_plain($class_description)) . '</p>';
    }
    $output .= '</dd>' . "\n";
  }

  $output .= '<dt class="form-label-cr">' . t('Registered streams') . '</dt>' . "\n";

  $form = drupal_get_form('channels_admin_stream_list', $id);
  
  $output .= '<dd>' . drupal_render($form) . '</dd>'; 

  $output .= '</dl>';

  return $output;
}



/**
 * Edit a channel stream's informations.
 *
 * @param object $stream
 *   The server to edit.
 */
function channels_admin_stream_edit(array $form, array &$form_state, ChannelStream $stream) {
  
  $title = t('Edit channel stream');
  drupal_set_title($title . ' "' . $stream->name . '"');
    
  $channel = channel_load($stream->channel_id);

  $form_state['values']['name'] = $stream->name;
  $form_state['values']['description'] = $stream->description;
  $form_state['values']['stream_type'] = $stream->type;
  $form_state['values']['bundle_name'] = $stream->bundle;
  $form_state['values']['publication_mode'] = $stream->options['publication_mode'];

  //$form_state['values']['service'] = $stream->service;
  
  $form = channels_admin_stream_add($form, $form_state, $channel);
  
  $form['stream'] = array(
    '#type' => 'hidden',
    '#value' => $stream->id,
  );

  return $form;
}


/**
 * Validation function for channels_admin_stream_edit.
 */
function channels_admin_stream_edit_submit(array $form, array &$form_state) {

  form_state_values_clean($form_state);

  $values = array(
    'id' => $form_state['values']['stream'],
    'name' => $form_state['values']['name'],
    'description' => $form_state['values']['description'],
    'options' => array('publication_mode' => $form_state['values']['publication_mode']),
  );

  $ret = channels_stream_edit($values);

  if ( !$ret ) {
    $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/stream/' . $form_state['values']['stream'] . '/edit';
    drupal_set_message(t('An error occured during stream edition.'), 'error');
  }
  else {
    $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/channel/' . $form_state['values']['channel'];
    drupal_set_message(t('The stream was successfully edited.'));
  }
}

/**
 * Add a channel stream.
 *
 * @param object $channel
 *   The channel to edit.
 */
function channels_admin_stream_add(array $form, array &$form_state, Channel $channel) {

  $title = t('Add a stream to channel');
  drupal_set_title($title . ' "' . $channel->name . '"');

  $stream_type = empty($form_state['values']['stream_type']) ? '' : $form_state['values']['stream_type'];
  $bundle_name = empty($form_state['values']['bundle_name']) ? '' : $form_state['values']['bundle_name'];
  $name = empty($form_state['values']['name']) ? '' : $form_state['values']['name'];
  $description = empty($form_state['values']['description']) ? '' : $form_state['values']['description'];
  $publication_mode = empty($form_state['values']['publication_mode']) ? '' : $form_state['values']['publication_mode'];

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Enter the displayed name for the current element.'),
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => $name,
    '#weight' => -2,
  );
  
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Description of the current element.'),
    '#default_value' => $description,
    '#weight' => -1,
  );  
  
  $form['stream_type'] = array(
    '#type' => 'select',
    '#title' => t('Type of stream'),
    '#required' => TRUE,
    '#description' => t('Choose the type of the stream you want to add.'),
    '#options' => array(
      '' => '< ' . t('Choose a type of stream') . ' >',
      'node' => t('Node'),
      'taxonomy_term' => t('Taxonomy'),
      'user' => t('User'),
    ),
    '#ajax' => array(
        'callback' => 'channels_admin_stream_add_callback',
        'wrapper' => 'bundle-name',
     ),
    '#default_value' => $stream_type,
    '#weight' => 0,
  );    
  
  $form['channel'] = array(
    '#type' => 'hidden',
    '#value' => $channel->id,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 20,
  ); 

  if ( $stream_type != '') {

    $form['ajax_form']['bundle_name'] = array(
      '#type' => 'select',
      '#title' => t('Bundle'),
      '#required' => TRUE,
      '#default_value' => $bundle_name,
      '#weight' => 1,
    );
    
    if ( $stream_type == 'node' || $stream_type == 'taxonomy_term' ) {

      $form['ajax_form']['bundle_name']['#options'] = array( '' => '< ' . t('Choose a bundle') . ' >');
      $form['ajax_form']['bundle_name']['#description'] = t('Please select the bundle to associate with this stream.');     
      
      // fill bundles select box with content types
      $info_bundles = field_info_bundles($stream_type);
      foreach ($info_bundles as $key => $info) {
        $form['ajax_form']['bundle_name']['#options'][$key] = $info['label'] . ' ( ' . $key . ' ) ';
      }    
    }
    else {
      $form['ajax_form']['bundle_name']['#options'] = array( $stream_type => $form['stream_type']['#options'][$stream_type] );
      $form['ajax_form']['bundle_name']['#disabled'] = TRUE;
    }
    
  }

  $form['publication_mode'] = array(
    '#type' => 'select',
    '#title' => t('Publication mode'),
    '#description' =>t('Choose how the publication is done'),
    '#options' => array(
        'user_select' => t('User can select if the content must be published on this channel or not'),
        'auto_select' => t('The content is automatically published on this channel, but the user can change this choice'),
        'force_select' => t('The content is automatically published on this channel, the user can\'t change this choice')
    ),
    '#default_value' => $publication_mode,
    '#weight' => 10,
  );

  $form['ajax_form']['bundle_name']['#prefix'] = '<div id="bundle-name">';
  $form['ajax_form']['bundle_name']['#suffix'] = '</div>';
  $form['ajax_form']['bundle_name']['#weight'] = 4;
  
  //$form = _channels_admin_service_form($form, $form_state);
  
  return $form;
}

/**
 * Validation function for channels_admin_stream_add.
 */
function channels_admin_stream_add_validate(array $form, array &$form_state) {}

/**
 * Submit function for channels_admin_stream_add.
 */
function channels_admin_stream_add_submit(array $form, array &$form_state) {

  form_state_values_clean($form_state);
  
  $id = channels_stream_insert(array(
    'type' => $form_state['values']['stream_type'],
    'bundle' => $form_state['values']['bundle_name'],
    'channel_id' => $form_state['values']['channel'],
    'name' => $form_state['values']['name'],
    'description' => $form_state['values']['description'],
    'options' => array('publication_mode' => $form_state['values']['publication_mode'])
    //'service' => $form_state['values']['service'],   
  ));
  
  $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/channel/' . $form_state['values']['channel'];
  
  if ( $id > 0)
    drupal_set_message(t('The stream was successfully created.'));
  else
    drupal_set_message(t('An error occured during the stream creation.'), 'error');
}


/**
 * Edit a channel stream's settings.
 *
 * @param object $stream
 *   The server to edit.
 */
function channels_admin_stream_settings(array $form, array &$form_state, ChannelStream $stream) {
  
  drupal_set_title(t('Edit channel stream "@name" settings', array('@name' => $stream->name) ));
  
  $options = $stream->getOptions();  
  
  $form_state['values']['bundle'] = $stream->bundle;
  $form_state['values']['type'] = $stream->type;
  
  $form_func = '_channels_admin_bundle_fields_' . $stream->type . '_form';
  
  if ( function_exists($form_func) )
    $form = $form_func($form, $form_state, $options);
  else{
    drupal_set_message(t('No settings form found for type "@type"', array('@type' => $stream->type)));
    return $form;
  }

  $form['stream'] = array(
    '#type' => 'hidden',
    '#value' => $stream->id,
  );  
    
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 5,
  ); 
  
  return $form;
}

/**
 * Validation function for channels_admin_stream_settings.
 */
function channels_admin_stream_settings_submit(array $form, array &$form_state) {

  form_state_values_clean($form_state);
  
  $stream = channel_stream_load($form_state['values']['stream']);
  
  // collect the submitted list of node properties
  $options['properties'] = array();
  foreach ($form_state['values']['properties'] as $key => $value) {
    if ( $value === $key) $options['properties'][] = $value;
  }
  
  // collect the submitted list of the content type's fields
  $options['fields'] = array();
  foreach ($form_state['values'][$stream->bundle . '-fields'] as $key => $value) {   
    if ( $value === $key  ) $options['fields'][] = $value; 
  }

  $ret = channels_stream_edit_options($stream->id, $options);
  
   if ($ret)
    drupal_set_message(t('The stream settings was successfully edited.'));
  else
    drupal_set_message(t('An error occured during the stream settings edition.'), 'error');

  $form_state['redirect'] = CHANNELS_MENU_BASEPATH . '/channel/' . $stream->channel_id;
}



/**
 * Theme function for displaying the channels publication options list.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: The form (table) of the streams (needed to re-order the synchronization priority).
 *   - channels: an array of Channel object, the channels to display the options.
 */
function theme_channels_admin_node_channels_options(array $variables) {
  
  $form = &$variables['form'];
  $channels = &$variables['channels'];


  if(isset($form['options']) && count($form['options'])>0) {

    $header = array('group' => t('Group'));
    foreach($channels as $key => $channel) {
        $channel = channel_load($channel->id);
        $channels[$key] = $channel;
        $header[$channel->machine_name] = $channel->name;
    }

    $rows = array();
    foreach($form['options'] as $key_opt => $option) {
      $row = array('group' => '');
      foreach($channels as $channel) {
        $checkbox = $option['channels_options_' . $channel->id . '_' . $key_opt];
        if(isset($checkbox['#group']) && isset($checkbox['#group']['name'])) $row['group'] = $checkbox['#group']['name'];
        $row[$channel->machine_name] = drupal_render($checkbox);
      }
      $rows[] = $row;
    }

    return theme_table(array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(),
        'caption' => '',
        'sticky' => '',
        'colgroups' => array(),
        'empty' => t('There is no channels publication options'),
    ));
  }
}


