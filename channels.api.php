<?php

/**
 * Returns a list of all (enabled) channels.
 *
 * @param $only_enabled
 *   Whether to retrieve only enabled servers.
 * @param $header
 *   A table header to sort by.
 *
 * @return array
 *   An array of objects representing all (or, if $enabled is TRUE, only
 *   enabled) search channels.
 */
function hook_channels_get_channels($only_enabled = FALSE, $header = NULL) {}


/**
 *
 * Return a list of channels for a given bundle
 *
 * @param string $bundle Bundle name
 * @param bool $only_enabled TRUE if you only want to check the enabled streams
 * @param array $header A table header to sort by
 *
 * @return array
 *   An array of objects representing all (or, if $enabled is TRUE, only
 *   enabled) search channels.
 */
function hook_channels_get_channels_by_bundle($bundle, $only_enabled = TRUE, $header = NULL) {}


/**
 *
 * Return a list of channels for a given node
 *
 * @param int $id Node id
 * @param bool $only_enabled TRUE if you only want to check the enabled streams
 * @param array $header A table header to sort by
 *
 * @return array
 *   An array of the channel's ids all (or, if $enabled is TRUE, only
 *   enabled) searched channels.
 */
function hook_channels_get_channels_by_node($id, $only_enabled = TRUE, $header = NULL) {}



/**
 *
 * Return a list of channels for a given entity
 *
 * @param int $id Entity id
 * @param string $type Entity type
 * @param bool $only_enabled TRUE if you only want to check the enabled streams
 * @param array $header A table header to sort by
 *
 * @return array
 *   An array representing all (or, if $enabled is TRUE, only
 *   enabled) searched channels.
 */
function hook_channels_get_channels_by_entity($id, $type, $only_enabled = TRUE, $header = NULL) {}


/**
 *
 * Return a list of assigned nodes for a given channel
 *
 * @param int $id Channel id
 * @param bool $only_enabled TRUE if you only want to check the enabled streams
 * @param array $header A table header to sort by
 *
 * @return array
 *   An array of objects representing all (or, if $enabled is TRUE, only
 *   enabled) searched nodes.
 */
function hook_channels_get_nodes_by_channel($id, $only_enabled = TRUE, $header = NULL) {}


/**
 * Inserts a new channel into the database.
 *
 * @param object $channel
 *   An object representing the inserted channel.
 *
 * @return
 *   The newly inserted channel's id, or FALSE on error.
 */
function hook_channels_channel_insert(array $values) {}


/**
 * Changes a channel's settings.
 *
 * @param object $channel
 *   An object representing the updated channel.
 *
 ** @param object $channel
 *   An object representing the channel before update.
 *
 */
function hook_channels_channel_update($channel, $channel_before) {}


/**
 * Inserts a new stream into the database.
 *
 * @param array $values
 *   An array containing the values to be inserted.
 *
 * @return
 *   The newly inserted stream's id, or FALSE on error.
 */
function hook_channels_stream_insert(array $values) {}


/**
 * Deletes a channel and delete all associated instances.
 *
 * @param object $channel
 *   The deleted channel.
 *
 * @return
 *   1 on success, 0 or FALSE on failure.
 */
function hook_channels_channel_delete($channel) {}


/**
 *
 * Add a node to a channel in database
 *
 * @param int $id Channel Id
 * @param int $node_id Node Id
 *
 * @return
 *   The newly inserted row's id, or FALSE on error.
 */
function hook_channels_channel_insert_entity($id, $node_id) {}


/**
 *
 *  Alter the node form after adding the options of the publication channels
 *
 * @param array $form
 * @param array $form_state
 * @param int $form_id
 */
function hook_channels_channel_alter_node_options($form, &$form_state, $form_id) {};


/**
 * Set the publication options list in the node form
 * @param string $type Content type
 * @param array $options Array of options
 *  Example:
 *
 * $options = array(
 *     0 => array(
 *        'name' => 'Group 1', //optional
 *        'value' => array(
 *         'option1' => array('#title' => 'Option 1', '#type' => 'checkbox'),
 *         'option2' => array('#title' => 'Option 2', '#type' => 'checkbox'),
 *         'option3' => array('#title' => 'Option 3', '#type' => 'checkbox'),
 *         'option4' => array('#title' => 'Option 4', '#type' => 'checkbox'),
 *       ),
 *     ),
 *     1 => array(
 *       'name' => 'Group 2', //optional
 *       'value' => array(
 *         'option5' => array('#title' => 'Option 5', '#type' => 'checkbox'),
 *         'option6' => array('#title' => 'Option 6', '#type' => 'checkbox'),
 *         'option7' => array('#title' => 'Option 7', '#type' => 'checkbox'),
 *         'option8' => array('#title' => 'Option 8', '#type' => 'checkbox'),
 *       ),
 *     ),
 *  );
 *
 */
function hook_channels_node_set_options($type, $options) {}