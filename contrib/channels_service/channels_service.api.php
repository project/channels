<?php

/**
 * Used before an item is processed
 * 
 * @param int $id Id of the processed item
 * @param array $item Array of the fields processed
 * @param array $context Context of the process
 */
function hook_channels_service_preprocess_item($id, array $item, $context) {}

/**
 *
 * Used after a successfull item processing
 *
 * @param int $channel_id Channel ID
 * @param string $type Type of entity (node, taxonomy_term...)
 * @param id $entity_id Entity ID
 */
function hook_channels_service_process_item_success($channel_id, $type, $entity_id){}


/**
 *
 * Used when an error occured during an item processing
 *
 * @param int $channel_id Channel ID
 * @param string $type Type of entity (node, taxonomy_term...)
 * @param id $entity_id Entity ID
 */
function hook_channels_service_process_item_error($channel_id, $type, $entity_id){}