<?php


define('CHANNELS_TAXONOMY_VOCABULARY_MACHINE_NAME', 'channels');

/**
 * Return the vocabulary machine_name associated to the channels
 */
function channels_taxonomy_get_vocabulary_machine_name() {
  return CHANNELS_TAXONOMY_VOCABULARY_MACHINE_NAME;
}

/**
 *
 *  Implements hook channels_channel_insert()
 *
 * @param object $channel
 */
function channels_taxonomy_channels_channel_insert($channel) {
  _channels_taxonomy_save_channel_as_term($channel);
  
}

/**
 *
 *  Implements hook channels_channel_update()
 *
 * @param object $channel
 * @param object $channel_before
 */
function channels_taxonomy_channels_channel_update($channel, $channel_before) {
  _channels_taxonomy_save_channel_as_term($channel, $channel_before);
}


/**
 *
 *  Implements hook channels_channel_delete()
 *
 * @param object $channel
 */
function channels_taxonomy_channels_channel_delete($channel) {
  $term = channels_taxonomy_term_name_load($channel->name);
  if(!is_null($term)){
    taxonomy_term_delete ($term->tid);
    drupal_set_message(t('The term "' . $term->name  . '" has been deleted from the vocabulary "' . CHANNELS_TAXONOMY_VOCABULARY_MACHINE_NAME . '"'));
  }
}

/**
 *
 * Save a channel as a taxonomy in the "channels" vocabulary
 *
 * @param array $channel Object representation of a channel
 */
function _channels_taxonomy_save_channel_as_term($channel, $channel_before = NULL) {

  $vocabulary = taxonomy_vocabulary_machine_name_load(CHANNELS_TAXONOMY_VOCABULARY_MACHINE_NAME);

  // update taxonomy
  if( !is_null($channel_before)) {
    $term = channels_taxonomy_term_name_load($channel_before->name);
  }
  else {
    $term = new stdClass();
    $term->format = NULL;
    $term->tid = NULL;
    $term->weight = 0;
  }
  $term->name = $channel->name;
  $term->description = $channel->description;
  $term->vid = $vocabulary->vid;

  $status = taxonomy_term_save($term);

  switch ($status) {
    case SAVED_NEW:
      drupal_set_message(t('A new term "' . $term->name  . '" has been added to the vocabulary "' . CHANNELS_TAXONOMY_VOCABULARY_MACHINE_NAME . '"'));
      watchdog('taxonomy', 'Created new term %term.', array('%term' => $term->name), WATCHDOG_NOTICE, l(t('edit'), 'taxonomy/term/' . $term->tid . '/edit'));
      break;

    case SAVED_UPDATED:
      drupal_set_message(t('The term "' . $term->name  . '" has been updated'));
      watchdog('taxonomy', 'Updated term %term.', array('%term' => $term->name), WATCHDOG_NOTICE, l(t('edit'), 'taxonomy/term/' . $term->tid . '/edit'));
      break;
  }

}

/**
 *
 *  Find and return a term by its name in the channels vocabulary
 *
 * @param string $name Term name
 * @return object Term object
 */
function channels_taxonomy_term_name_load($name) {
  $vocabulary = taxonomy_vocabulary_machine_name_load(CHANNELS_TAXONOMY_VOCABULARY_MACHINE_NAME);
  $terms = taxonomy_term_load_multiple(array(), array('name' => trim($name), 'vid' => $vocabulary->vid));
  if(count($terms)>0) return array_pop ($terms);
  return NULL;
}

/**
 * Return the channel associated to a given term
 * @param int $tid Term ID
 * @return object Channel object or NULL
 */
function channels_taxonomy_get_channel_by_term($tid) {

  $term = taxonomy_term_load($tid);

  $ret = entity_load('channel', FALSE, array('name' => $term->name), FALSE);
  return $ret ? array_shift($ret) : NULL;
}


/**
 * Return the term correponding to a givent channel
 *
 * @param int $channel_id Channel ID
 *
 * @return object $term
 */
function channels_taxonomy_get_channel_term($channel_id) {
  $channel = channel_load($channel_id);
  $term = channels_taxonomy_term_name_load($channel->name);
  return $term;
}


/**
 *
 *  Implements hook node_submit()
 * @param int $node
 * @param array $form
 * @param array $form_state
 */
/*function channels_taxonomy_node_submit($node, $form, &$form_state) {

    if( isset($node->channels_options) && count($node->channels_options)>0 && isset($node->field_channels)) {
      
      $langcode = field_valid_language(NULL);
      if(!isset($node->field_channels[$langcode])){
        $langcode = 'und';
        if(!isset($node->field_channels[$langcode])) $node->field_channels[$langcode] = array();
      }

      $attr_terms = array();
      foreach ($node->field_channels[$langcode] as $channel) {
        $attr_terms[] = $channel['tid'];
      }

      foreach($node->channels_options as $chid => $value) {
        $term = channels_taxonomy_get_channel_term($chid);
        
        // the channel is enabled
        if($value['enabled'] == 1) {
          // the channel is not in the taxonomy
          if(!in_array($term->tid, $attr_terms))
            $node->field_channels[$langcode][] = (array) $term;
        }
        elseif(in_array($term->tid, $attr_terms)) {
          foreach($node->field_channels[$langcode] as $key => $channel)
            if($channel['tid'] == $term->tid) unset($node->field_channels[$langcode][$key]);
        }
        
      }
  }
}*/

/**
 *
 *  Implements hook_channels_service_process_item_success($channel_id, $entity_type, $entity_id)
 *
 * @param int $channel_id Channel ID
 * @param string $entity_type Type of the entity (node, taxonomy_term, ...)
 * @param int $entity_id Entity ID
 */
function channels_taxonomy_channels_service_process_item_success($channel_id, $entity_type, $entity_id) {

  $entities = entity_load($entity_type, array($entity_id));

  if(count($entities)>0) {
    $entity = current($entities);
  }
  else return FALSE;

  if(isset($entity->field_channels)) {

    $langcode = field_valid_language(NULL);

    if($entity->status == 1) {

      if(!isset($entity->field_channels[$langcode]))
        $entity->field_channels[$langcode] = array();

      $attributed_terms = array();
      foreach ($entity->field_channels[$langcode] as $channel) {
        $attributed_terms[] = $channel['tid'];
      }

      //foreach($node->channels_options as $chid => $value) {
      $term = channels_taxonomy_get_channel_term($channel_id);
      
      if(!is_null($term)) {

        if(in_array($term->tid, $attributed_terms)) {
          foreach ($entity->field_channels[$langcode] as $key => $channel) {
            if($channel['tid'] == $term->tid) 
              unset($entity->field_channels[$langcode][$key]);
          }
        }

        $entity->field_channels[$langcode][] = (array) $term;
      }
    }
    else{
      $entity->field_channels[$langcode] = array();
    }    
  }
}


