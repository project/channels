<?php

/**
 * @file
 * Class representing a channel.
 */
class Channel extends EntityDBExtended {
  

  /**
   * 
   * Entity type
   * @var string
   */
  protected $entityType = 'channel';

  /**
	 * 
	 * Return entity info
	 * 
	 * @access public 
	 * @return array
	 */
  public static function getEntityInfo() {
    return array(
      'label' => t('Channel'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Channel',
      'base table' => 'channel',
      'load hook' => 'channel_load',
      'uri callback' => 'channel_url',
        'entity keys' => array(
        'id' => 'id',
        'name' => 'machine_name',
      )
    );
  }

  /**
   * 
   * Enable the current channel
   * @return bool
   */
  public function enable() {

    $ret = db_update($this->entityType())
      ->fields(array('status' => 1))
      ->condition('id', $this->values['id'])
      ->execute();

    if (!$ret) return $ret;

    $this->values['status'] = 1;  

    //TODO specific work
  
    return TRUE;
  }
  
  /**
   * 
   * Disable the current channel
   * @return bool
   */
  public function disable() {
    
    $ret = db_update($this->entityType())
      ->fields(array('status' => 0))
      ->condition('id', $this->values['id'])
      ->execute();
  
    if (!$ret) return $ret;

    $this->values['status'] = 0;  

    //TODO specific work  
    return TRUE;
  }
  
  /**
   * 
   * Return true if the current channel is enabled
   */
  public function isEnabled() {
    if ($this->values['status'] == 1) return TRUE;
    return FALSE;
  }
  
  
  /**
   * 
   * Return the number of channel's streams
   * 
   * @return int $nb Number of channel's streams
   */
  public function getNbStreams() {

    $rows = db_select('channel_stream', 'c')
      ->fields('c')
      ->condition('channel_id', $this->values['id'])
      ->execute();
        
    return $rows->rowCount();
  }
  
  
  /**
   * 
   * Return the streams of the current channel 
   * 
   * @param bool $only_enabled
   * @param array $header
   */
  public function fetchStreams($only_enabled = FALSE, $header = NULL) {

    $stream = new ChannelStream();

    if ( isset($this->values['id']))
      return $stream->fetchByChannel($this->values['id'], $only_enabled, $header);

    return $stream->fetchAll($only_enabled, $header);
  }

  /**
   * 
   * Return the streams of the current channel for a given type
   * @param string $type Type name
   * @param bool $only_enabled
   * @param array $header
   */
  public function fetchStreamsByTypeBundle($type, $bundle, $only_enabled = FALSE, $header = NULL) {

    $stream = new ChannelStream();

    $conditions = array('type' => $type, 'bundle' => $bundle);

    if (isset($this->values['id'])) $conditions['channel_id'] = $this->values['id'];

    return $stream->fetchByConditions($conditions, $only_enabled, $header);
  }
  
  
  
  
  
}