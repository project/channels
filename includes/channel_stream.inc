<?php

/**
 * @file
 * Class representing a channel stream.
 */
class ChannelStream extends EntityDBExtended {

  /**
	 * 
	 * Entity type
	 * @var string
	 */
  protected $entityType = 'channel_stream';

  /**
   * 
   * Return entity info
   * 
   * @access public 
   * @return array
   */
  public static function getEntityInfo() {
    return array(
      'label' => t('Channel Stream'),
      'controller class' => 'EntityAPIController',
      'entity class' => 'ChannelStream',
      'base table' => 'channel_stream',
      'load hook' => 'channel_stream_load',
      'uri callback' => 'channel_stream_url',
        'entity keys' => array(
          'id' => 'id'
        )
    );
  }
  
  /**
   * 
   * Enable the current stream
   * @return bool
   */
  public function enable() {
    
    $ret = db_update($this->entityType())
      ->fields(array('status' => 1))
      ->condition($this->idKey, $this->values['id'])
      ->execute();
      
    if (!$ret)
      return $ret;
      
    $this->values['status'] = 1;  
    
    //TODO specific work
  
  return TRUE;
    
  }  
  
  /**
   * 
   * Disable the current stream
   * @return bool
   */
  public function disable() {
    
    $ret = db_update($this->entityType())
      ->fields(array('status' => 0))
      ->condition($this->idKey, $this->values['id'])
      ->execute();
      
    if (!$ret)
      return $ret;
      
    $this->values['status'] = 0;  
    
    //TODO specific work  
    return TRUE;
    
  }
  
  /**
   * 
   * Return a rowset of all streams
   * 
   * @param bool $only_enabled Is set to "TRUE" to select only enabled streams
   * @param array $header
   * 
   * @return array $elts Array of ChannelStream
   */
  public function fetchAll($only_enabled = FALSE, $header = NULL) {

    $enabled = (int) $only_enabled;

    $select = db_select($this->entityType, 'ce', array('fetch' => 'ChannelStream'))->fields('ce');
    if ($enabled) $select->condition('ce.status', 1);
    
    if (!empty($header))
      $select = $select->extend('TableSort')->orderByHeader($header);

    $results = $select->execute();
    $elts = array();
    foreach ($results as $row) {
      $elts[] = $row;
    }

    return $elts;
  }

   /**
   *
   * Return a rowset of all streams
   *
   * @param bool $only_enabled Is set to "TRUE" to select only enabled streams
   * @param array $header
   *
   * @return array $elts Array of ChannelStream
   */
  public function fetchByConditions($conditions = array(), $only_enabled = FALSE, $header = NULL) {

    $enabled = (int) $only_enabled;

    $select = db_select($this->entityType, 'ce', array('fetch' => 'ChannelStream'))->fields('ce');
    if ($enabled) $select->condition('ce.status', 1);

    foreach($conditions as $field => $value) $select->condition('ce.' . $field, $value);

    if (!empty($header))
      $select = $select->extend('TableSort')->orderByHeader($header);

    $results = $select->execute();
    $elts = array();
    foreach ($results as $row) { $elts[] = $row; }

    return $elts;
  }
  
  /**
   * 
   * Return a rowset of all elements
   * 
   * @param int $channel_id Channel id
   * @param bool $only_enabled Is set to "TRUE" to select only enabled elements
   * @param array $header
   * 
   * @return array $elts Array of ChannelStream
   */
  public function fetchByChannel($channel_id, $only_enabled = FALSE, $header = NULL) {
    
    $enabled = (int) $only_enabled;
    
    $select = db_select($this->entityType, 'ce', array('fetch' => 'ChannelStream'))->fields('ce');
    if ($enabled) {
      $select->condition('ce.status', 1);
    }
    $select->condition('ce.channel_id', $channel_id);
    
    if (!empty($header)) {
      $select = $select->extend('TableSort')->orderByHeader($header);
    }

    $results = $select->execute();
    $elts = array();
    foreach ($results as $row) {
      $elts[] = $row;
    }

    return $elts;
  }

  
  /**
   * 
   * Return the current element options
   * 
   * @param string $idx Index of the array to return
   * @return array $options
   */
  public function getOptions($idx = '') {

    if ( !isset($this->values['options']))
      return array();

    if ( is_string($this->values['options']))
      $this->values['options'] = unserialize($this->values['options']);

    // if no index -> return all options
    if ( $idx == '' ) return $this->values['options'];
    elseif ( isset($this->values['options'][$idx]) )
      return $this->values['options'][$idx];
    return array();
  }
  
  /**
   * 
   * Set the current stream options
   * 
   * @param array $options Stream options
   */
  public function setOptions($options) {
    $this->values['options'] = $options;
  }

  /**
   * 
   * Return true if the current channel element is enabled
   */
  public function isEnabled() {
    if ($this->values['status'] == 1) return TRUE;
    return FALSE;
  }




  /**
   * Permanently saves the stream.
   *
   * @see entity_save()
   *
   * @return int
   *   - number of row affected in case of update
   *   - new row id in case of insertion, or 0 if an error occured
   */
  public function save() {

    if( isset($this->values['options']) )
      $this->values['options'] = serialize ($this->values['options']);

    $today = new DateTime();

    if ( isset($this->values[$this->idKey]) ) {

      $this->values['changed'] = $today->format('Y-m-d H:i:s');
      return $this->update($this->values[$this->idKey], $this->values);
    }
    else {
      $this->values['created'] = $today->format('Y-m-d H:i:s');
      return $this->insert($this->values);
    }
  }

}