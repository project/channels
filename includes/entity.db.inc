<?php

/**
 * @file
 * Class representing a table model.
 */
class EntityDBExtended extends Entity {

  /**
   * Current row instance values
   * @var array
   */
  protected $values = array();
  
  
  /**
   * 
   * Return entity info
   * 
   * @access public 
   * @return array
   */
  public static function getEntityInfo() {

    return array(
      'label' => '',
      'controller class' => 'EntityAPIController',
      'entity class' => '',
      'base table' => '',
      'load hook' => '',
      'uri callback' => '',
      'entity keys' => NULL
    );
  }
  
  /**
   * Constructor as a helper to the parent constructor.
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, $this->entityType());        
  }
  
  
  // Magic methods for property magic

  public function __get($name) {
    return $this->values[$name];
  }

  public function __set($name, $value) {
    $this->values[$name] = $value;
  }

  public function __isset($name) {
    return isset($this->values[$name]);
  }

  public function __unset($name) {
    unset($this->values[$name]);
  }  
  
  public function getValues() {
    return $this->values;
  }
  
  /**
   * 
   * Update a row in the current table
   * 
   * @access public
   * @static
   * @param int $id Row id
   * @param array $fields Fields to update (associative array)
   * 
   * @return int $ret Return the number of rows affected by the update action 
   */
  public function update($id, $fields) {
    
    // If there are no new values, just return 0.
    if (empty($fields)) return FALSE;
    
    $ret = db_update($this->entityType)
    ->fields($fields)
    ->condition('id', $id)
    ->execute();
    
    if (!$ret) return 0;
    
    return $ret;
  }
  
  /**
   * Permanently saves the entity.
   *
   * @see entity_save()
   * 
   * @return int
   *   - number of row affected in case of update
   *   - new row id in case of insertion, or 0 if an error occured
   */
  public function save() {

    $today = new DateTime();

    if ( isset($this->values[$this->idKey]) ) {

      $this->values['changed'] = $today->format('Y-m-d H:i:s');
      return $this->update($this->values[$this->idKey], $this->values);
    }
    else {
      $this->values['created'] = $today->format('Y-m-d H:i:s');
      return $this->insert($this->values);
    }
  }
  
  /**
   * 
   * Insert a new row
   * @param array $fields Array of the fields to insert
   * 
   * @return int $id The new row id, 0 if an error occured
   */
  public function insert($fields) {

    if ( isset($fields[$this->idKey]) ) return 0;

    return db_insert($this->entityType)->fields($fields)->execute();
  }

  
  /**
   * Permanently deletes the entity.
   *
   * @see entity_delete()
   */
  public function delete() {
    
    if (isset($this->values[$this->idKey])) {

      return db_delete($this->entityType)
        ->condition($this->idKey, $this->values[$this->idKey])
        ->execute();
    }
    return FALSE;
  }
}