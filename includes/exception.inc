<?php

/**
 * @file
 * Represents an exception or error that occurred in some part of the Channel module
 * framework.
 */
class ChannelException extends Exception {

  /**
   * Creates a new ChannelException.
   *
   * @param $message
   *   A string describing the cause of the exception.
   */
  public function __construct($message = NULL) {
    if (!$message) {
      $message = t('An error occcurred in the Channel module.');
    }
    parent::__construct($message);
  }

}
