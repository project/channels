<?php
// $Id: channels_servic_dummy.inc,v 1.39 2010/08/25 09:15:08 abouillis Exp $

/**
 * @file
 * XMLRPC Service implementation
 */

class ChannelsServiceDummy extends ChannelsServiceInstance {


  protected static $MACHINE_NAME = 'dummy';

  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {

    
    $settings_form['channels_service_settings'] = array(
      '#type' => 'textfield',
      '#title' => 'Test',
    );

    return $settings_form;
  }

  public function  sendEntity($type, $id, $item) {

  }

  public function  deleteEntity($type, $id) {

  }

  public function  entityExists($id, $type) {
    
  }

  protected function sendAttachment($file, $entity_id, $field_name){}

  public function  typeIsSupported($type, $bundle) {
    
  }
}