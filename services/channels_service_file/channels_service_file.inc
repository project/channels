<?php
// $Id: channels_service_xmlrpc.inc,v 1.2 2011/01/19 16:15:45 abouillis Exp $

/**
 * @file
 * XMLRPC Service implementation
 */

class ChannelsServiceFile extends ChannelsServiceInstance {

  protected $session = null;
  
  protected static $SETTINGS = array();
  
  protected static $MACHINE_NAME = 'file';


    /**
   *
   *  Log a debug messagepublic class channel
   *
   * @static
   * @param string $message
   * @param array $variables
   */
  protected static function  debug($message, $variables = array()) {
    parent::debug(self::$MACHINE_NAME . ' | ' . $message, $variables);
  }


  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    
    $service_settings = isset($thread->options['form']['channels_services']['form']['services']['file']['form']['settings']['file_sync']) ? $thread->options['form']['channels_services']['form']['services']['file']['form']['settings']['file_sync'] : array();

    $services_version = isset($service_settings['services_version']) ? $service_settings['services_version'] : '';
    
    $file_sync_local_enabled = (isset($service_settings['local']['params']['enabled'])) ? $service_settings['local']['params']['enabled'] : '';
    $file_sync_local_copy_dest_dir = (isset($service_settings['local']['params']['dest_dir'])) ? $service_settings['local']['params']['dest_dir'] : '';
    
    $file_sync_ssh2_enabled = (isset($service_settings['ssh2']['params']['enabled'])) ? $service_settings['ssh2']['params']['enabled'] : 0;
    $file_sync_ssh2_remote_dir = (isset($service_settings['ssh2']['params']['remote_dir'])) ? $service_settings['ssh2']['params']['remote_dir'] : '';
    $file_sync_ssh2_host = (isset($service_settings['ssh2']['params']['host'])) ? $service_settings['ssh2']['params']['host'] : '';
    $file_sync_ssh2_port = (isset($service_settings['ssh2']['params']['port'])) ? $service_settings['ssh2']['params']['port'] : '22';
    $file_sync_ssh2_user = (isset($service_settings['ssh2']['params']['user'])) ? $service_settings['ssh2']['params']['user'] : '';
    $file_sync_ssh2_pwd = (isset($service_settings['ssh2']['params']['pwd'])) ? $service_settings['ssh2']['params']['pwd'] : '';
        
    $settings_form = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      'file_sync' => array(
              
        'local' => array(
          '#type' => 'fieldset',
          '#title' => t('Local copy'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          'params' => array(
          
            'enabled' => array(
              '#type' => 'checkbox',
              '#title' => t('Enabled'),
              '#default_value' => $file_sync_local_enabled,
            ),
          
            'dest_dir' => array(
              '#type' => 'textfield',
              '#title' => t('Local destination directory'),
              '#default_value' => $file_sync_local_copy_dest_dir,
              '#description' => t('Destination directory where we want to copy the files'),
              '#attributes' => array('placeholder' => '/home/my_directory/files'),
            ),
          )
        ),
        
        'ssh2' => array(
          '#type' => 'fieldset',
          '#title' => t('SSH2 remote copy'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          'params' => array(
          
            'enabled' => array(
              '#type' => 'checkbox',
              '#title' => t('Enabled'),
              '#default_value' => $file_sync_ssh2_enabled,
            ),
            
            'host' => array(
              '#type' => 'textfield',
              '#title' => t('Host name'),
              '#default_value' => $file_sync_ssh2_host,
              '#attributes' => array('placeholder' => 'srv1.server.net'),
              '#description' => t('Domain name or IP of the server without any protocol')
            ),
            'port' => array(
              '#type' => 'textfield',
              '#title' => t('Port'),
              '#default_value' => $file_sync_ssh2_port,
              '#description' => ''
            ),
            'user' => array(
              '#type' => 'textfield',
              '#title' => t('User name'),
              '#default_value' => $file_sync_ssh2_user,
              '#description' => t('User name on the remote server to use for copy')
            ),
            'pwd' => array(
              '#type' => 'textfield',
              '#title' => t('Password'),
              '#default_value' => $file_sync_ssh2_pwd,
              '#description' => t('User password to connect the server'),
            ),  
            'remote_dir' => array(
              '#type' => 'textfield',
              '#title' => t('Remote directory'),
              '#default_value' => $file_sync_ssh2_remote_dir,
              '#description' => t('Remote directory where we want to copy the files'),
              '#attributes' => array('placeholder' => '/home/my_directory/files'),
            ),
          )
        ),
      ),
    );
    
    
    return $settings_form;
  }
  
  
  
  public function initConnection($host, $port, $user, $password, $sftp_mode = false) {
    static $con;
        
    if(is_null($con)) {
      $con = ssh2_connect($host, $port);
      if(!$con) {
        watchdog('error', 'unable to initialize SSH2 connection to '.$host.':'.$port);
        return NULL;
      }
      if(!ssh2_auth_password($con, $user, $password)) {
        watchdog('error', 'unable to initialize SSH2 connection to '.$host.':'.$port. ' -> wrong username or password');
        return NULL;
      }
    }
    if($sftp_mode) {
      $con = ssh2_sftp($con);
    }
    return $con;
  }
  
  /**
   * Allow process engines to do some init stuff. 
   * 
   * @param ProcessApiThread $thread
   *
   * @return bool
   *
   *      TRUE if the beginning of the proccess successed
   *      FALSE it the beginning of the proccess failed
   *
   */
  public function threadBeginProcess(ProcessApiThread $thread) {
    
    self::$SETTINGS = isset($thread->options['form']['channels_services']['form']['services']['file']['form']['settings']) ? $thread->options['form']['channels_services']['form']['services']['file']['form']['settings'] : array();
    
    foreach(self::$SETTINGS['file_sync'] as $file_sync_type => $file_sync) {
      
      if($file_sync_type == 'local' && $file_sync['params']['enabled']) {
        if(!channels_service_file_local_mkdir($file_sync['params']['dest_dir'])) {
          watchdog('error', 'unable to create destination directory');
          return FALSE; 
        }
      }
      elseif($file_sync_type == 'ssh2' && $file_sync['params']['enabled']) {
        
        $con = $this->initConnection($file_sync['params']['host'], (int)$file_sync['params']['port'], $file_sync['params']['user'], $file_sync['params']['pwd']);
        //var_dump($con);
        //exit;
        //if(ssh2_sftp_mkdir($con, $file_sync['params']['remote_dir'])) self::debug('remote directory created: '.$file_sync['params']['remote_dir']);
        if(ssh2_exec($con, ' mkdir -m 0777 '.$file_sync['params']['remote_dir'])) 
          self::debug('remote directory created: '.$file_sync['params']['remote_dir']);
      }
    }
    
    return TRUE;
  }
  
  /**
   * 
   * Send an entity to the configured service in process api module
   * 
   * @param string $type The type of entity (node, user,...)
   * @param int $id The ID of the entity
   * @param array $item The list of fields to save
   * @return bool
   *  Return TRUE if the entity is successfully sent
   *  Return FALSE if an error occured
   */  
  protected function sendEntity($type, $id, $item) {
    
    $errors = array();
    $filename = $id.'-'.$item['type']['value'].'.'.$type;
    $tmpDir = '/tmp/channels/ssh';
    if(!is_dir($tmpDir)) mkdir($tmpDir, 0777, true);
    
    foreach(self::$SETTINGS['file_sync'] as $file_sync_type => $file_sync) {
    
      // local copy
      if($file_sync_type == 'local' && $file_sync['params']['enabled']) {
  
        $local_file = $file_sync['params']['dest_dir'].'/'.$filename;
        $res_node = file_put_contents($local_file, serialize($item));
        if(!$res_node) {
          $errors[] = 'unable to create file '.$file->filename;
        }
          
      }
      elseif($file_sync_type == 'ssh2' && $file_sync['params']['enabled']) {
        
        if(self::$SETTINGS['file_sync']['local']['params']['enabled'] && 
        isset(self::$SETTINGS['file_sync']['local']['params']['dest_dir']) && 
        self::$SETTINGS['file_sync']['local']['params']['dest_dir']  != '')
          $local_file = self::$SETTINGS['file_sync']['local']['params']['dest_dir'].'/'.$filename;
        else
          $local_file = $tmpDir.'/'.$filename;
        
        if(!file_put_contents($local_file, serialize($item))) {
          $errors[] = 'unable to create file '.$file->filename;
        }
        else {
          $remote_file = $file_sync['params']['remote_dir'].'/'.$filename;
        
        $con = $this->initConnection($file_sync['params']['host'], (int)$file_sync['params']['port'], $file_sync['params']['user'], $file_sync['params']['pwd']);      
        if(file_exists($local_file)) {
          //if(!ssh2_sftp_mkdir(ssh2_sftp($con), dirname($remote_file), 0775, true)) $errors[] = 'unable to create remote directory '.dirname($remote_file);
          if(!ssh2_exec($con, ' mkdir -m 0777 '.dirname($remote_file))) $errors[] = 'unable to create remote directory '.dirname($remote_file);
          if(!ssh2_scp_send($con, $local_file, $remote_file)) $errors[] = 'unable to create remote directory '.$remote_file;
        }
        else $errors[] = 'file not exists '.$local_file;

        }
        
        
      }
    }
    
    return TRUE;    
  }


  /**
   *
   * Delete an entity from the configured service in process api module
   *
   * @param string $type The type of entity (node, user,...)
   * @param int $id The ID of the entity
   * @return bool
   *  Return TRUE if the entity is successfully sent
   *  Return FALSE if an error occured
   */
  protected function deleteEntity($type, $id) {
    return TRUE;
  }



  /**
   *
   *  Test if an entity already exists on remote service
   *
   * @param int $id Entity ID
   * @param string $type Entity type (node, taxonomy_term...)
   *
   * @return bool
   *    TRUE if the entity already exists
   *    FALSE ig the entity doesn't exist
   */
  protected function entityExists($id, $type) {    
    return FALSE;
  }
  
  /**
   *
   * Check if the given type is supported by the remote service
   *
   * @param string $type Entity type (node, taxonomy_term...)
   * @param string $bundle Entity bundle (article, page, ...)
   * @return bool
   *        TRUE if the type is supported
   *        FALSE if the type is not supported
   *
   */
  protected function typeIsSupported($type, $bundle) {
    return TRUE;
  }

  /**
   * Send an entity attachment as a file to the service
   *
   * @param object $file Object representation of a file
   *
   * Example:
   * stdClass Object
   *  (
   *             [fid] => 18
   *             [uid] => 1
   *             [filename] =>
   *             [uri] => public://field/image/imagefield_nUslG8.png
   *             [filemime] => image/png
   *             [filesize] => 1303
   *             [status] => 1
   *             [timestamp] => 1295884715
   *  )
   * @param int $id Entity ID
   * @param string $field_name The field's name the file is attached
   * @return bool
   *  Return TRUE if the attachment has been successfully sent
   *  Return FALSE if an error occured during the attachment send
   *
   */
  protected function sendAttachment($file, $id, $field_name){

    $errors = array();
    $tmpDir = '/tmp/channels/ssh';
    if(!is_dir($tmpDir)) mkdir($tmpDir, 0777, true);

    foreach(self::$SETTINGS['file_sync'] as $file_sync_type => $file_sync) {
      
      // local copy
      if($file_sync_type == 'local' && $file_sync['params']['enabled']) {
        
        $dest_filename = preg_split('/\./', $file->uri);
        
        if(isset($dest_filename[1])) {
          
          $dest_file = preg_replace('/public:\/\//', $file_sync['params']['dest_dir'].'/', $file->uri);
          $dir = dirname($dest_file);
          if(!is_dir($dir)) mkdir(dirname($dest_file), 0775, true);
          if(!copy(drupal_realpath($file->uri), $dest_file)) {
            $errors[] = 'unable to create file '.$dest_file;
          }
          
          /*$res_metadata = file_put_contents($file_sync['params']['dest_dir'].'/'.$file->fid.'.file', serialize((array)$file));
          if(!$res_metadata) {
            $errors[] = 'unable to create metadata for file '.$file->filename;
          }*/
          
        }
      }
      elseif($file_sync_type == 'ssh2' && $file_sync['params']['enabled']) {
        
        // remote copy
        $remote_file = preg_replace('/public:\/\//', $file_sync['params']['remote_dir'].'/', $file->uri);
        $con = $this->initConnection($file_sync['params']['host'], (int)$file_sync['params']['port'], $file_sync['params']['user'], $file_sync['params']['pwd']);     
        
        //ssh2_sftp_mkdir(ssh2_sftp($con), dirname($remote_file), 0775, true);
          ssh2_exec($con, ' mkdir -pm 0777 '.dirname($remote_file));
        
        
          //$errors[] = 'unable to create remote directory '.dirname($remote_file);
        if(!ssh2_scp_send($con, drupal_realpath($file->uri), $remote_file)) 
          $errors[] = 'unable to create remote directory '.$remote_file;
        
        if(self::$SETTINGS['file_sync']['local']['params']['enabled'] && isset(self::$SETTINGS['file_sync']['local']['params']['dest_dir']) && self::$SETTINGS['file_sync']['local']['params']['dest_dir'] != '') {
          $local_dir = self::$SETTINGS['file_sync']['local']['params']['dest_dir'];
        }
        else $local_dir = $tmpDir;
        
        /*$filename = $file->fid.'.file';
        $local_path = $local_dir . '/' . $filename;
       
        if(!file_exists($local_path)) {
          if(!file_put_contents($local_path, serialize((array)$file))) 
            $errors[] = 'unable to create metadata for file '.$filename;          
        }

        if(file_exists($local_path)){
          $remote_file =  $file_sync['params']['remote_dir'].'/'.$filename;
          if(!ssh2_scp_send($con, $local_path, $remote_file)) $errors[] = 'unable to create remote directory '.$remote_file;  
        }*/
         
      }
    }

    if(empty($errors)) return TRUE;    
    else foreach($errors as $error) watchdog('error', $error);

    return FALSE;
  }

}