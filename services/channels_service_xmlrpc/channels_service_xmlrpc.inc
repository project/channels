<?php
// $Id: channels_service_xmlrpc.inc,v 1.2 2011/01/19 16:15:45 abouillis Exp $

/**
 * @file
 * XMLRPC Service implementation
 */

class ChannelsServiceXmlrpc extends ChannelsServiceInstance {

  protected $session = null;
  
  protected static $SETTINGS = array();
  
  protected static $MACHINE_NAME = 'xmlrpc';

    /**
   *
   *  Log a debug messagepublic class channel
   *
   * @static
   * @param string $message
   * @param array $variables
   */
  protected static function  debug($message, $variables = array()) {
    $variables = array_merge(array('!machine_name' => self::$MACHINE_NAME), $variables);    
    parent::debug('!machine_name | ' . $message, $variables);
  }


  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    
    $service_settings = isset($thread->options['form']['channels_services']['form']['services']['xmlrpc']['form']['settings']['form']) ? $thread->options['form']['channels_services']['form']['services']['xmlrpc']['form']['settings']['form'] : array();

    $services_version = isset($service_settings['services_version']) ? $service_settings['services_version'] : '';
    $xmlrpc_url = isset($service_settings['xmlrpc_url']) ? $service_settings['xmlrpc_url'] : '';
    $xmlrpc_key = isset($service_settings['xmlrpc_key']) ? $service_settings['xmlrpc_key'] : '';
    $xmlrpc_domain = isset($service_settings['xmlrpc_domain']) ? $service_settings['xmlrpc_domain'] : $_SERVER['HTTP_HOST'];
    
    $xmlrpc_user = isset($service_settings['xmlrpc_user']) ? $service_settings['xmlrpc_user'] : ''; 
    $xmlrpc_user_pass = isset($service_settings['xmlrpc_user_pass']) ? $service_settings['xmlrpc_user_pass'] : '';  
    
    $settings_form = array(
      '#type' => 'fieldset',
      '#title' => 'XMLRPC Service settings',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'form' => array(
        /*'services_version' => array(
            '#type' => 'select',
            '#title' => t('Services module version'),
            '#description' => t('Please choose the version of the Services module on the client'),
            '#options' => array(
              '6_2_3' => '6.x-2.3',
              '6_2_2' => '6.x-2.2',
            ),
            '#default_value' => $services_version,
        ),*/
        'xmlrpc_url' => array(
            '#type' => 'textfield',
            '#title' => t('XMLRPC Service URL'),
            '#description' => 'http://my_host_name/xmlrpc',
            '#default_value' => $xmlrpc_url,
            '#size' => 100,
            '#maxlength' => 255,
            //'#required' => TRUE,
        ),
        'xmlrpc_key' => array(
            '#type' => 'textfield',
            '#title' => t('XMLRPC Service key'),
            '#description' => t('Sample of key: e3880ad3e757b3f93daff0ffc68a15d4'),
            '#default_value' => $xmlrpc_key,
            '#size' => 30,
            '#maxlength' => 255,
            //'#required' => TRUE,
        ),
        'xmlrpc_domain' => array(
          '#type' => 'textfield',
          '#title' => t('XMLRPC Domain name associated to the service key'),
          '#default_value' => $xmlrpc_domain,
          '#size' => 25,
          '#maxlength' => 50,
          //'#required' => TRUE,
        ),
        'xmlrpc_user' => array(
          '#type' => 'textfield',
          '#title' => t('XMLRPC User name'),
          '#description' => t('Name of a user authorized to use the service'),
          '#default_value' => $xmlrpc_user,
          '#maxlength' => 60,
          '#size' => 25,
          //'#required' => TRUE,
        ),
        'xmlrpc_user_pass' => array(
          '#type' => 'textfield',
          '#title' => t('XMLRPC User pasword'),
          '#description' => t('Password of the user.'),
          '#default_value' => $xmlrpc_user_pass,
          '#maxlength' => 128,
          '#size' => 25,
          //'#required' => TRUE,
        )
      ),
    );
    return $settings_form;
  }
  
  
  public function initConnection($thread) {
    
    // session initialization
    if( is_null($this->session['sessid']) ) 
      $this->session['sessid'] = $this->connect();

    // check if the session is initialized on first connection
    if( is_null($this->session['sessid']) ) {
      self::log($thread, 0, 'error', t('Unable to process item: no connection.'));
      $this->closeConnection();
      return FALSE;
    }

    // channels service user login
    if( !isset ($this->session['user']) ) $this->session = $this->login();

    // check if the session is initialized
    if( is_null($this->session) ) {
      self::log($thread, 0, 'error', t('Unable to process item: no connection.'));
      $this->closeConnection();
      return FALSE;
    }

    return TRUE;
  }
  
  /**
   * Allow process engines to do some init stuff. 
   * 
   * @param ProcessApiThread $thread
   *
   * @return bool
   *
   *      TRUE if the beginning of the proccess successed
   *      FALSE it the beginning of the proccess failed
   *
   */
  public function threadBeginProcess(ProcessApiThread $thread) {
    
    self::$SETTINGS = $thread->options['form']['channels_services']['form']['services'][self::$MACHINE_NAME]['form']['settings']['form'];

    return TRUE;
  }


  public function threadProcessItem(ProcessApiThread $thread, $id, array $item, $context) {
    
    if(!$this->initConnection($thread)) return FALSE;    
    return parent::threadProcessItem($thread, $id, $item, $context);
  }
    
    /**
   * Allow process engines to do some cleanup stuff. 
   * 
   * @param ProcessApiThread $thread
   */
  public function threadEndProcess(ProcessApiThread $thread) {
    self::closeConnection();
  }


  /**
   * 
   * Prepare the common connection parameters
   *
   * @param string $method_name Method name to call
   */
  protected function getParameters($method_name) {
    
    $timestamp = (string) time();
    $host = self::$SETTINGS['xmlrpc_domain'];
    $nonce = user_password();
    $hash_parameters = array($timestamp, $host, $nonce, $method_name);
    $hash = hash_hmac("sha256", implode(';', $hash_parameters),  self::$SETTINGS['xmlrpc_key']);

    $parameters = array(
      'hash' => $hash,
      'domain_name' => $host,
      'domain_time_stamp' => $timestamp,
      'nonce' => $nonce,
      'sessid' => $this->session['sessid'],
    );
    return array(); // remove to use key authentication
    return $parameters;
  }
  
  
  /**
   *
   * XML-RPC Request - Close a connection
   *
   * @return mixed FALSE if an error occured, or TRUE if the connection is closed
   */
  protected function closeConnection() {

    if(!is_null($this->session)) {
      $xmlrpc_result = xmlrpc(
        self::$SETTINGS['xmlrpc_url'],
        array(
          'user.logout' => $this->getParameters('user.logout'),
        ),
        array('timeout' => 60)
      );

      if ($xmlrpc_result === FALSE) {
       return FALSE;
      }
      self::debug('XML-RPC: connection closed');
      $this->session = NULL;
      return TRUE;
    }
  }


  /**
   *
   * XML-RPC Request - Initialize connection
   *
   * @static
   * @return mixed NULL if an error occured, or the connection ID as a string
   */
  protected function connect() {

    $xmlrpc_result = xmlrpc(
      self::$SETTINGS['xmlrpc_url'],
      array(
        'system.connect' => array()
      ),
      array('timeout' => 60)
    );
    
    if ($xmlrpc_result === FALSE) {

      watchdog(
        'channels',
        t('unable to connect to: !url (!error)', array(
          '!url' =>self::$SETTINGS['xmlrpc_url'],
          '!error' => xmlrpc_error_msg()
        )),
        array(), WATCHDOG_ERROR);
      
      return NULL;
    }

    return $xmlrpc_result['sessid'];
  }

  /**
   *
   * XMLRPC User login
   * @return mixed Return NULL if the login failed, or the user account as an array if the user login succeed.
   */
  protected function login() {

    $parameters = $this->getParameters('user.login');
    $parameters = array_merge(
      $parameters,
      array(
        'username' => self::$SETTINGS['xmlrpc_user'],
        'password' => self::$SETTINGS['xmlrpc_user_pass'],
      )
    );

    /*if(isset(self::$SETTINGS['services_version']) && self::$SETTINGS['services_version'] == '6_2_2')
      unset($parameters['sessid']);*/
    //watchdog('debug', $parameters['sessid']);

    $xmlrpc_result = xmlrpc(
      self::$SETTINGS['xmlrpc_url'],
      array(
        'user.login' => $parameters,
      ),
      array('timeout' => 60)
    );

    if ($xmlrpc_result === FALSE) {

      watchdog(
        'channels',
        t('An error occured during user login for "!user": !error', array(
          '!user' =>self::$SETTINGS['xmlrpc_user'],
          '!error' => xmlrpc_error_msg()
        )),
        array(), WATCHDOG_ERROR);
      return NULL;
    }

    self::debug('XML-RPC: new connection, user !user connected, sessid: !sessid', array('!user' => self::$SETTINGS['xmlrpc_user'], '!sessid' => $xmlrpc_result['sessid']));

    return  $xmlrpc_result;
  }

  
  /**
   *
   * Check if the given type is supported by the remote service
   *
   * @param string $type Entity type (node, taxonomy_term...)
   * @param string $bundle Entity bundle (article, page, ...)
   * @return bool
   *        TRUE if the type is supported
   *        FALSE if the type is not supported
   *
   */
  protected function typeIsSupported($type, $bundle) {

    self::debug('!type !bundle: verifying that it is supported',array('!type' => $type, '!bundle' => $bundle));

    $parameters = $this->getParameters('channels.entitySupported');

    $xmlrpc_result = xmlrpc(
      self::$SETTINGS['xmlrpc_url'],
      array(
        'channels.entitySupported' => array_merge(
          $parameters,
          array('type' => $type, 'bundle' => $bundle)
        ),
      ),
      array('timeout' => 60)
    );    

    if($xmlrpc_result ===  FALSE){

      watchdog(
        'channels',
        t('Unable to verify if the type is supported: !error', array(
          '!error' => xmlrpc_errno()
        )),
        array(), WATCHDOG_ERROR);
      return FALSE;

    }
    return TRUE;
  }

  /**
   * 
   * Send an entity to the configured service in process api module
   * 
   * @param string $type The type of entity (node, user,...)
   * @param int $id The ID of the entity
   * @param array $item The list of fields to save
   * @return bool
   *  Return TRUE if the entity is successfully sent
   *  Return FALSE if an error occured
   */  
  protected function sendEntity($type, $id, $item) {

    // replace the content's author with the xmlrpc user
    $item['uid'] = array('name' => 'User ID', 'type' => 'integer','value' => $this->session['user']['uid']);

    self::debug('!type !item: sending', array('!type' => $type, '!item' => $id));

    $parameters = $this->getParameters('channels.saveEntity');

    $xmlrpc_result = xmlrpc(
      self::$SETTINGS['xmlrpc_url'], 
      array(
        'channels.saveEntity' => array_merge(
          $parameters,
          array('type' => $type, 'id' => $id, 'fields' => $item)
        ),
      ),
      array('timeout' => 60)
    );

    if ($xmlrpc_result === FALSE) {

      $this->_error = xmlrpc_error_msg();
      
      watchdog(
        'channels', 
        t('Unable to send entity !entity: !error', array(
          '!entity' => $id, 
          '!error' => xmlrpc_error_msg()
        )),
        array('type' => $type, 'id' => $id, 'fields' => $item),
        WATCHDOG_ERROR
      );
      return FALSE;      
    }
    
    self::debug('!type !item: successfully sent', array('!type' => $type, '!item' => $id));
    
    return TRUE;    
  }


  /**
   *
   * Delete an entity from the configured service in process api module
   *
   * @param string $type The type of entity (node, user,...)
   * @param int $id The ID of the entity
   * @return bool
   *  Return TRUE if the entity is successfully sent
   *  Return FALSE if an error occured
   */
  protected function deleteEntity($type, $id) {

    self::debug('!type !item: deleting', array('!type' => $type, '!item' => $id));

    $xmlrpc_result = xmlrpc(
      self::$SETTINGS['xmlrpc_url'],
      array(
        'channels.deleteEntity' => array_merge(
          $this->getParameters('channels.deleteEntity'),
          array('type' => $type, 'id' => $id)
        ),
      ),
      array('timeout' => 60)
    );

    if ($xmlrpc_result === FALSE) {

      $this->_error = xmlrpc_error_msg();

      watchdog(
        'channels',
        t('Unable to delete entity !entity: !error', array(
          '!entity' => $id,
          '!error' => xmlrpc_error_msg()
        )),
        array('type' => $type, 'id' => $id),
        WATCHDOG_ERROR
      );
      //return FALSE;
      return TRUE;
    }

    self::debug('!type !item: successfully sent', array('!type' => $type, '!item' => $id));

    return TRUE;
  }



  /**
   *
   *  Test if an entity already exists on remote service
   *
   * @param int $id Entity ID
   * @param string $type Entity type (node, taxonomy_term...)
   *
   * @return bool
   *    TRUE if the entity already exists
   *    FALSE ig the entity doesn't exist
   */
  protected function entityExists($id, $type) {

    $parameters = $this->getParameters('channels.entityExists');

    //watchdog('debug', $parameters['sessid']);

    $xmlrpc_result = xmlrpc(
      self::$SETTINGS['xmlrpc_url'],
      array(
        'channels.entityExists' => array_merge(
          $parameters,
          array( 'id' => $id, 'type' => $type)
        ),
      ),
      array('timeout' => 60)
    );

    if ($xmlrpc_result === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Send an entity attachment as a file to the service
   *
   * @param object $file Object representation of a file
   *
   * Example:
   * stdClass Object
   *  (
   *             [fid] => 18
   *             [uid] => 1
   *             [filename] =>
   *             [uri] => public://field/image/imagefield_nUslG8.png
   *             [filemime] => image/png
   *             [filesize] => 1303
   *             [status] => 1
   *             [timestamp] => 1295884715
   *  )
   * @param int $id Entity ID
   * @param string $field_name The field's name the file is attached
   * @return bool
   *  Return TRUE if the attachment has been successfully sent
   *  Return FALSE if an error occured during the attachment send
   *
   */
  protected function sendAttachment($file, $id, $field_name){

    if( isset($file->uri)) {
      
      $filepath = drupal_realpath($file->uri);
      $file->filepath = file_uri_target($file->uri);
      $binaryfile = fopen($filepath, 'rb');
      $file->file = base64_encode(fread($binaryfile, filesize($filepath)));
      fclose($binaryfile);

      $parameters = $this->getParameters('channels.saveFile');

      $xmlrpc_result = xmlrpc(
        self::$SETTINGS['xmlrpc_url'],
        array(
          'channels.saveFile' => array_merge(
            $parameters,
            array( 'file' => $file, 'id' => $id, 'field_name' => $field_name, 'file_hash' => md5(serialize($file)))
          ),
        ),
        array('timeout' => 60)
      );
      
      if ($xmlrpc_result === FALSE) {
        self::debug('unable to send attachment, fid !fid: !error', array('!fid' => $file->fid, '!error' => xmlrpc_error_msg()));
        return FALSE;
      }
    }
    else{
      self::debug('unable to send attachment, fid !fid: !error', array('!fid' => $file->fid, '!error' => 'no uri'));
      return FALSE;
    }
    return TRUE;
  }

}